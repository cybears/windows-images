# Windows Images

This repository is used to build Windows based images which are not
redistributable. Specifically, things like the Visual Studio based builder
image require us to accept a license which forbids redistribution.
